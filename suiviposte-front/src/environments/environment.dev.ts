export const environment = {
    production: false,
    suiviBackendBaseUrl : "http://localhost:8080/suiviposte",
    connexionUrl : "/user/connexion",
    ajoutSuiviUrl : "/suivi/addSuivi",
    suppressionSuiviUrl : "/suivi/deleteSuivi",
    modificationSuiviUrl : "/suivi/editSuivi",
    inscriptionUrl : "/user/subscribe"
};