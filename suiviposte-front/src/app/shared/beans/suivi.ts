export class Suivi {
    idSuivi : Number;
	
	codeStatut : Number;
	
	libelleStatut : String;
	
	codeSuivi : String;

	notifEmail : Boolean;
	
    notifTelephone : Boolean;
}