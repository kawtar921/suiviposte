import { Suivi } from './suivi';

export class User {
    username : String;
    password : String;
    firstname: String;
    lastname : String;
    email: String;
    telephone: String;
	suivis : Array<Suivi>;
}