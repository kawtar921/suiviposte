import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class HttpService<T> {

    constructor(private http: HttpClient){

    }

    public get(url : string, params? : any)  : Observable<T> {
        let urlService = environment.suiviBackendBaseUrl
        
        let httpParams : HttpParams
        if(params){
            httpParams = new HttpParams()
            Object.keys(params).map(key => {
                console.log(key + " : " + params[key])
                httpParams = httpParams.append(key, params[key])
            })
        }
        return this.http.get<T>(urlService + url,params ? {params : httpParams} : {})
    }

    public post(url : string, entity : T) {
        let urlService = environment.suiviBackendBaseUrl
        return this.http.post(urlService + url,entity);
    }

    public postWithListResponse(url : string, entity : T) : Observable<T[]>{
        let urlService = environment.suiviBackendBaseUrl
        return this.http.post<T[]>(urlService + url,entity);
    }

    public delete(url : string, entity : T) {
        let urlService = environment.suiviBackendBaseUrl
        return this.http.request('delete', urlService + url, {body: entity})
    }

    public put(url : string, entity : T) {
        let urlService = environment.suiviBackendBaseUrl
        return this.http.request('put', urlService + url, {body: entity})
    }
}