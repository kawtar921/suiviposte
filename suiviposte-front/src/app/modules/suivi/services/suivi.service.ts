import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Connexion } from 'src/app/shared/beans/connexion';
import { Suivi } from 'src/app/shared/beans/suivi';
import { User } from 'src/app/shared/beans/user';
import { HttpService } from 'src/app/shared/services/http.service';
import { environment } from '../../../../environments/environment';

@Injectable()
export class SuiviService{

    constructor(private httpService : HttpService<Suivi>,
        private router: Router){
    }

    public ajoutSuivi(suiviBean : Suivi) : Observable<Suivi[]>  {
        let user  : User = JSON.parse(sessionStorage.getItem('user'))
        let url = environment.ajoutSuiviUrl + "/" + user.username
        return this.httpService.postWithListResponse(url,suiviBean);
    }

    public suppressionSuivi(suiviBean : Suivi)  : Observable<any> {
        let user  : User = JSON.parse(sessionStorage.getItem('user'))
        let url = environment.suppressionSuiviUrl + "/" + user.username
        console.log("suppression bean = " , suiviBean)
        return this.httpService.delete(url,suiviBean)
    }

    public modificationSuivi(suiviBean : Suivi) : Observable<any> {
        let user  : User = JSON.parse(sessionStorage.getItem('user'))
        let url = environment.modificationSuiviUrl + "/" + user.username
        console.log("modification bean = " , suiviBean)
        return this.httpService.put(url,suiviBean)
    }

    public setSuivisSession(suivis : Suivi[]) {
        let user  : User = JSON.parse(sessionStorage.getItem('user'))
        user.suivis = suivis
        sessionStorage.setItem('user' , JSON.stringify(user))
    }

    
}