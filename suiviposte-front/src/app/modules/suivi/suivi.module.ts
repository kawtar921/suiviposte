import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuiviMainComponent } from './components/suivi-main/suivi-main.component';
import { MatTableModule } from '@angular/material/table';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { SuiviAjoutComponent } from './components/suivi-ajout/suivi-ajout.component';
import { SuiviService } from './services/suivi.service';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SuiviSuppressionComponent } from './components/suivi-suppression/suivi-suppression.component';
import { SuiviModificationComponent } from './components/suivi-modification/suivi-modification.component';



@NgModule({
  declarations: [SuiviMainComponent, SuiviAjoutComponent, SuiviSuppressionComponent, SuiviModificationComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCheckboxModule

  ],
  providers : [
    SuiviService
  ],
  entryComponents : [
    SuiviAjoutComponent,
    SuiviSuppressionComponent,
    SuiviModificationComponent
  ]
})
export class SuiviModule { }
