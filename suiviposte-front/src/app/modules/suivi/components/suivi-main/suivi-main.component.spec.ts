import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiviMainComponent } from './suivi-main.component';

describe('SuiviMainComponent', () => {
  let component: SuiviMainComponent;
  let fixture: ComponentFixture<SuiviMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiviMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiviMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
