import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Suivi } from 'src/app/shared/beans/suivi';
import { User } from 'src/app/shared/beans/user';
import { SuiviService } from '../../services/suivi.service';
import { SuiviAjoutComponent } from '../suivi-ajout/suivi-ajout.component';
import { SuiviModificationComponent } from '../suivi-modification/suivi-modification.component';
import { SuiviSuppressionComponent } from '../suivi-suppression/suivi-suppression.component';


@Component({
  selector: 'app-suivi-main',
  templateUrl: './suivi-main.component.html',
  styleUrls: ['./suivi-main.component.scss']
})
export class SuiviMainComponent implements OnInit {

  public user : User
  public dataSource : any
  public displayedColumns : string[]
  constructor(public dialog: MatDialog,
    private suiviService : SuiviService,
    private router : Router) { }

  ngOnInit() {
    this.refreshTable()
  }

  addSuivi() {
    this.dialog.open(SuiviAjoutComponent).afterClosed().subscribe(
      (suivi) => {
        this.suiviService.ajoutSuivi(suivi).subscribe(
          (suivis) => {
            this.suiviService.setSuivisSession(suivis)
            this.refreshTable()
          },
          (error) => {
              console.log("errror : " , error)
          }
      )
        
      });
  }

  refreshTable() {
    this.user = JSON.parse(sessionStorage.getItem('user'))

    this.dataSource = new MatTableDataSource(this.user.suivis)
    this.displayedColumns = ['idSuivi', 'codeSuivi', 'statutSuivi', 'notifSuivi', 'controleSuivi'];
  }

  supprimerSuivi(suivi : Suivi){
    this.dialog.open(SuiviSuppressionComponent).afterClosed().subscribe(
      (response) => {
        if(response) {
          //suppression du suivi
          this.suiviService.suppressionSuivi(suivi).subscribe(
            (suivis) => {
              this.suiviService.setSuivisSession(suivis)
              this.refreshTable()
            }
          )
        }
      },
      (error) => {
        console.log("erreur = " , error)
      }
    )
  }

  modifierSuivi(suivi : Suivi) {
    this.dialog.open(SuiviModificationComponent, { data : suivi}).afterClosed().subscribe(
      (suiviModal) => {
          //modification du suivi
          suivi.codeSuivi = suiviModal.codeSuivi
          suivi.notifEmail = suiviModal.notifEmail
          suivi.notifTelephone = suiviModal.notifTelephone
          
          this.suiviService.modificationSuivi(suivi).subscribe(
            (suivis) => {
              this.suiviService.setSuivisSession(suivis)
              this.refreshTable()
            }
          )
      },
      (error) => {
        console.log("erreur = " , error)
      }
    )
  }

  deconnexion() {
    sessionStorage.removeItem('user')
    this.router.navigate(['/authentification'])
  }
}
