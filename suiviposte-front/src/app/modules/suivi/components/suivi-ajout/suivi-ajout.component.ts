import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Suivi } from 'src/app/shared/beans/suivi';

@Component({
  selector: 'app-suivi-ajout',
  templateUrl: './suivi-ajout.component.html',
  styleUrls: ['./suivi-ajout.component.scss']
})
export class SuiviAjoutComponent implements OnInit {

  suivi : Suivi = new Suivi()
  ajoutSuiviForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<SuiviAjoutComponent>) { }

  ngOnInit() {
    this.setFormControls();
  }

  public setFormControls(){
    this.ajoutSuiviForm = this.formBuilder.group({
      codeSuivi: [this.suivi.codeSuivi, Validators.required],
      notifEmail: [this.suivi.notifEmail],
      notifTelephone : [this.suivi.notifTelephone]
    })
  }

  public invalidateForm(){
    Object.keys(this.ajoutSuiviForm.controls).forEach(field => {
      const control = this.ajoutSuiviForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public ajouterSuivi() {
    if (this.ajoutSuiviForm.valid) {
      this.suivi = this.ajoutSuiviForm.value;
      this.dialogRef.close(this.suivi)

    } else {
      this.invalidateForm();
    }
    
  }

}
