import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiviAjoutComponent } from './suivi-ajout.component';

describe('SuiviAjoutComponent', () => {
  let component: SuiviAjoutComponent;
  let fixture: ComponentFixture<SuiviAjoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiviAjoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiviAjoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
