import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiviSuppressionComponent } from './suivi-suppression.component';

describe('SuiviSuppressionComponent', () => {
  let component: SuiviSuppressionComponent;
  let fixture: ComponentFixture<SuiviSuppressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiviSuppressionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiviSuppressionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
