import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Suivi } from 'src/app/shared/beans/suivi';

@Component({
  selector: 'app-suivi-suppression',
  templateUrl: './suivi-suppression.component.html',
  styleUrls: ['./suivi-suppression.component.scss']
})
export class SuiviSuppressionComponent implements OnInit {

  suppressionSuiviForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public suivi: Suivi,
  public dialogRef: MatDialogRef<SuiviSuppressionComponent>) { }

  ngOnInit() {
  }

  
  public supprimerSuivi() {
      this.dialogRef.close(true)
  }






}
