import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Suivi } from 'src/app/shared/beans/suivi';

@Component({
  selector: 'app-suivi-modification',
  templateUrl: './suivi-modification.component.html',
  styleUrls: ['./suivi-modification.component.scss']
})
export class SuiviModificationComponent implements OnInit {

  modificationSuiviForm: FormGroup;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public suivi: Suivi,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<SuiviModificationComponent>
  ) { }

  ngOnInit() {
    this.setFormControls();
  }

  public setFormControls(){
    this.modificationSuiviForm = this.formBuilder.group({
      codeSuivi: [this.suivi.codeSuivi, Validators.required],
      notifEmail: [this.suivi.notifEmail],
      notifTelephone : [this.suivi.notifTelephone]
    })
  }

  public invalidateForm(){
    Object.keys(this.modificationSuiviForm.controls).forEach(field => {
      const control = this.modificationSuiviForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public modifierSuivi() {
    if (this.modificationSuiviForm.valid) {
      this.suivi = this.modificationSuiviForm.value;
      this.dialogRef.close(this.suivi)

    } else {
      this.invalidateForm();
    }
  }

}
