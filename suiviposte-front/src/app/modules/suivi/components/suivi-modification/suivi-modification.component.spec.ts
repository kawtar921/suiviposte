import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiviModificationComponent } from './suivi-modification.component';

describe('SuiviModificationComponent', () => {
  let component: SuiviModificationComponent;
  let fixture: ComponentFixture<SuiviModificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiviModificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiviModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
