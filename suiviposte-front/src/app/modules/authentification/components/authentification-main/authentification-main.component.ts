import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Connexion } from 'src/app/shared/beans/connexion';
import { ConnexionService } from '../../services/connexion.service';

@Component({
  selector: 'app-authentification-main',
  templateUrl: './authentification-main.component.html',
  styleUrls: ['./authentification-main.component.scss']
})
export class AuthentificationMainComponent implements OnInit {

  connexionBean : Connexion = new Connexion()
  userConnexionForm: FormGroup;
  constructor(
    private connexionService : ConnexionService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.setFormControls();
  }

  public connexion() {
    console.log("connexion ...")
    if (this.userConnexionForm.valid) {
      this.connexionBean = this.userConnexionForm.value;
      console.log(this.connexionBean);

      this.connexionService.connexion(this.connexionBean)

    } else {
      this.invalidateForm();
    }
    
  }

  public setFormControls(){
    this.userConnexionForm = this.formBuilder.group({
      username: [this.connexionBean.username, Validators.required],
      password: [this.connexionBean.password, [Validators.required]]
    })
  }

  public invalidateForm(){
    Object.keys(this.userConnexionForm.controls).forEach(field => {
      const control = this.userConnexionForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  public inscription() {
    this.router.navigate(['/inscription'])
  }


}
