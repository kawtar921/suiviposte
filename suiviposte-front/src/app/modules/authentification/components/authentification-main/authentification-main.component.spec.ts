import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthentificationMainComponent } from './authentification-main.component';

describe('AuthentificationMainComponent', () => {
  let component: AuthentificationMainComponent;
  let fixture: ComponentFixture<AuthentificationMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthentificationMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthentificationMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
