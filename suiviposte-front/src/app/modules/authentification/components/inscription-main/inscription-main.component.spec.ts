import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionMainComponent } from './inscription-main.component';

describe('InscriptionMainComponent', () => {
  let component: InscriptionMainComponent;
  let fixture: ComponentFixture<InscriptionMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriptionMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
