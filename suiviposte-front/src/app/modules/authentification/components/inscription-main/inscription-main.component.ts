import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/shared/beans/user';
import { ConnexionService } from '../../services/connexion.service';

@Component({
  selector: 'app-inscription-main',
  templateUrl: './inscription-main.component.html',
  styleUrls: ['./inscription-main.component.scss']
})
export class InscriptionMainComponent implements OnInit {

  user : User = new User()
  userInscriptionForm: FormGroup;

  constructor(private connexionService : ConnexionService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.setFormControls();
  }

  inscription() {
    if (this.userInscriptionForm.valid) {
      this.user = this.userInscriptionForm.value;

      this.connexionService.inscription(this.user)

    } else {
      this.invalidateForm();
    }
  }

  public setFormControls(){
    this.userInscriptionForm = this.formBuilder.group({
      username: [this.user.username, Validators.required],
      password: [this.user.password, [Validators.required]],
      firstname: [this.user.firstname, [Validators.required]],
      lastname: [this.user.lastname, [Validators.required]],
      email: [this.user.email, [Validators.required]],
      telephone: [this.user.telephone, [Validators.required]],
    })
  }

  public invalidateForm(){
    Object.keys(this.userInscriptionForm.controls).forEach(field => {
      const control = this.userInscriptionForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

}
