import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Connexion } from 'src/app/shared/beans/connexion';
import { User } from 'src/app/shared/beans/user';
import { HttpService } from 'src/app/shared/services/http.service';
import { environment } from '../../../../environments/environment';



@Injectable()
export class ConnexionService {

    constructor(
        private httpService : HttpService<User>,
        private router: Router ){

    }

    public connexion(connexionBean : Connexion) {
        let params = {
            "username" : connexionBean.username,
            "password" : connexionBean.password
        }
        
        this.httpService.get(environment.connexionUrl , params)
        .subscribe (
            (user : User) => {
                sessionStorage.setItem('user' , JSON.stringify(user))
                
                //redirection vers page suivi
                this.router.navigate(['/suivi'])
            },
            (error) => {
                console.log(error)
            }
        )
        ;
    }


    public inscription(user : User) {
        this.httpService.post(environment.inscriptionUrl, user).subscribe(
            (user) => {
                sessionStorage.setItem('user' , JSON.stringify(user))

                //redirection vers page suivi
                this.router.navigate(['/suivi'])
            },
            (error) => {
                console.log(error)
            }
        )
    }

}