import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthentificationMainComponent } from './components/authentification-main/authentification-main.component';
import { FormsModule } from '@angular/forms';
import { ConnexionService } from './services/connexion.service';
import { HttpService } from 'src/app/shared/services/http.service';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { InscriptionMainComponent } from './components/inscription-main/inscription-main.component';

@NgModule({
  declarations: [AuthentificationMainComponent, InscriptionMainComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCardModule
  ],
  providers : [
    ConnexionService,
    HttpService,
    
  ]
})
export class AuthentificationModule { }
