import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthentificationMainComponent } from './modules/authentification/components/authentification-main/authentification-main.component';
import { InscriptionMainComponent } from './modules/authentification/components/inscription-main/inscription-main.component';
import { SuiviMainComponent } from './modules/suivi/components/suivi-main/suivi-main.component';
import { AuthentificationGuard } from './shared/guards/authentification.guard';

const routes: Routes = [
  { path : 'authentification' , component : AuthentificationMainComponent} , 
  { path : 'suivi' , component : SuiviMainComponent, canActivate : [AuthentificationGuard]} , 
  { path : 'inscription' , component : InscriptionMainComponent} , 
  { path : '' , redirectTo : '/authentification' , pathMatch : 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
