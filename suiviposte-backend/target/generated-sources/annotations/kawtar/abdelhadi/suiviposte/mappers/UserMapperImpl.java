package kawtar.abdelhadi.suiviposte.mappers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import kawtar.abdelhadi.suiviposte.dto.SuiviDTO;
import kawtar.abdelhadi.suiviposte.dto.UserDTO;
import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-24T21:25:22+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Autowired
    private SuiviMapper suiviMapper;

    @Override
    public UserDTO toUserDTO(User user) {
        if ( user == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setUsername( user.getUsername() );
        userDTO.setFirstname( user.getFirstname() );
        userDTO.setLastname( user.getLastname() );
        userDTO.setEmail( user.getEmail() );
        userDTO.setTelephone( user.getTelephone() );
        userDTO.setSuivis( suiviMapper.toListeSuiviDTO( user.getSuivis() ) );

        return userDTO;
    }

    @Override
    public User toUser(UserDTO userDTO) {
        if ( userDTO == null ) {
            return null;
        }

        User user = new User();

        user.setUsername( userDTO.getUsername() );
        user.setFirstname( userDTO.getFirstname() );
        user.setLastname( userDTO.getLastname() );
        user.setEmail( userDTO.getEmail() );
        user.setTelephone( userDTO.getTelephone() );
        user.setSuivis( suiviDTOListToSuiviList( userDTO.getSuivis() ) );

        return user;
    }

    protected List<Suivi> suiviDTOListToSuiviList(List<SuiviDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Suivi> list1 = new ArrayList<Suivi>( list.size() );
        for ( SuiviDTO suiviDTO : list ) {
            list1.add( suiviMapper.toSuivi( suiviDTO ) );
        }

        return list1;
    }
}
