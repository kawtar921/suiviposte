package kawtar.abdelhadi.suiviposte.mappers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import kawtar.abdelhadi.suiviposte.dto.SuiviDTO;
import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.enums.StatutSuivi;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-24T21:25:22+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
@Component
public class SuiviMapperImpl implements SuiviMapper {

    @Override
    public SuiviDTO toSuiviDTO(Suivi suivi) {
        if ( suivi == null ) {
            return null;
        }

        SuiviDTO suiviDTO = new SuiviDTO();

        suiviDTO.setCodeStatut( suiviStatutId( suivi ) );
        suiviDTO.setLibelleStatut( suiviStatutLibelle( suivi ) );
        suiviDTO.setIdSuivi( suivi.getIdSuivi() );
        suiviDTO.setCodeSuivi( suivi.getCodeSuivi() );
        suiviDTO.setNotifEmail( suivi.getNotifEmail() );
        suiviDTO.setNotifTelephone( suivi.getNotifTelephone() );

        return suiviDTO;
    }

    @Override
    public List<SuiviDTO> toListeSuiviDTO(List<Suivi> suivis) {
        if ( suivis == null ) {
            return null;
        }

        List<SuiviDTO> list = new ArrayList<SuiviDTO>( suivis.size() );
        for ( Suivi suivi : suivis ) {
            list.add( toSuiviDTO( suivi ) );
        }

        return list;
    }

    @Override
    public Suivi toSuivi(SuiviDTO suiviDTO) {
        if ( suiviDTO == null ) {
            return null;
        }

        Suivi suivi = new Suivi();

        suivi.setIdSuivi( suiviDTO.getIdSuivi() );
        suivi.setCodeSuivi( suiviDTO.getCodeSuivi() );
        suivi.setNotifEmail( suiviDTO.getNotifEmail() );
        suivi.setNotifTelephone( suiviDTO.getNotifTelephone() );

        suivi.setStatut( StatutSuivi.getStatutSuiviById(suiviDTO.getCodeStatut()) );

        return suivi;
    }

    private Long suiviStatutId(Suivi suivi) {
        if ( suivi == null ) {
            return null;
        }
        StatutSuivi statut = suivi.getStatut();
        if ( statut == null ) {
            return null;
        }
        Long id = statut.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private String suiviStatutLibelle(Suivi suivi) {
        if ( suivi == null ) {
            return null;
        }
        StatutSuivi statut = suivi.getStatut();
        if ( statut == null ) {
            return null;
        }
        String libelle = statut.getLibelle();
        if ( libelle == null ) {
            return null;
        }
        return libelle;
    }
}
