package kawtar.abdelhadi.suiviposte.mappers;

import javax.annotation.Generated;
import kawtar.abdelhadi.suiviposte.dto.ExceptionDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-24T21:25:22+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
@Component
public class ExceptionMapperImpl extends ExceptionMapper {

    @Override
    public ExceptionDTO toExceptionDTO(String message, int code) {

        ExceptionDTO exceptionDTO = new ExceptionDTO();

        if ( message != null ) {
            exceptionDTO.setMessage( message );
        }
        exceptionDTO.setCodeError( String.valueOf( code ) );

        ExceptionDTO target = doAfterMapping( message, code, exceptionDTO );
        if ( target != null ) {
            return target;
        }

        return exceptionDTO;
    }
}
