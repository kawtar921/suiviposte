package kawtar.abdelhadi.suiviposte.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kawtar.abdelhadi.suiviposte.dto.UserDTO;
import kawtar.abdelhadi.suiviposte.entities.User;
import kawtar.abdelhadi.suiviposte.exceptions.AuthorizationException;
import kawtar.abdelhadi.suiviposte.exceptions.NotFoundException;
import kawtar.abdelhadi.suiviposte.mappers.ExceptionMapper;
import kawtar.abdelhadi.suiviposte.mappers.UserMapper;
import kawtar.abdelhadi.suiviposte.services.UserService;

@RestController
@RequestMapping(path = "/user")
public class UserEndpoint {

	@Autowired UserService userService;
	@Autowired UserMapper userMapper;
	@Autowired ExceptionMapper exceptionMapper;
	
	@GetMapping(value = "/connexion")
	public ResponseEntity<UserDTO> connectUser(@RequestParam("username") String username, @RequestParam("password") String password) throws NotFoundException, AuthorizationException {
			User user = this.userService.connect(username, password);
			return ResponseEntity.ok(this.userMapper.toUserDTO(user));
	}
	
	@PostMapping(value = "/subscribe")
	public ResponseEntity<UserDTO> subscribeUser(@RequestBody User user) {
			User newUser = this.userService.addOrUpdateUser(user);
			return ResponseEntity.ok(this.userMapper.toUserDTO(newUser));
	}
	
}
