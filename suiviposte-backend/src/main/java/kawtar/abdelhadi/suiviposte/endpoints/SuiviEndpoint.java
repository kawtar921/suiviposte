package kawtar.abdelhadi.suiviposte.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kawtar.abdelhadi.suiviposte.beans.PosteAPIBean;
import kawtar.abdelhadi.suiviposte.dto.SuiviDTO;
import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.exceptions.AuthorizationException;
import kawtar.abdelhadi.suiviposte.exceptions.NotFoundException;
import kawtar.abdelhadi.suiviposte.mappers.SuiviMapper;
import kawtar.abdelhadi.suiviposte.services.PosteAPIService;
import kawtar.abdelhadi.suiviposte.services.SuiviService;

@RestController
@RequestMapping(path = "/suivi")
public class SuiviEndpoint {
	
	@Autowired SuiviService suiviService;
	@Autowired SuiviMapper suiviMapper; 
	@Autowired PosteAPIService posteAPIService;
	
	@GetMapping(value = "/allSuivis/{username}")
	public ResponseEntity<List<SuiviDTO>> getAllSuivis(@PathVariable("username") String username) throws NotFoundException, AuthorizationException {
		List<Suivi> suivis = suiviService.getAllSuiviByUserId(username);
		return ResponseEntity.ok(this.suiviMapper.toListeSuiviDTO(suivis));
	}
	
	@PostMapping(value = "/addSuivi/{username}")
	public ResponseEntity<List<SuiviDTO>> addSuivi(@PathVariable("username") String username, @RequestBody Suivi suivi) throws NotFoundException, AuthorizationException {
		List<Suivi> suivis = suiviService.addSuivi(username, suivi);
		return ResponseEntity.ok(this.suiviMapper.toListeSuiviDTO(suivis));
	}

	@PutMapping(value = "/editSuivi/{username}")
	public ResponseEntity<List<SuiviDTO>> editiSuivi(@PathVariable("username") String username, @RequestBody SuiviDTO suivi) throws NotFoundException, AuthorizationException {
		List<Suivi> suivis = suiviService.editSuivi(username, this.suiviMapper.toSuivi(suivi));
		return ResponseEntity.ok(this.suiviMapper.toListeSuiviDTO(suivis));
	}
	
	@DeleteMapping(value = "/deleteSuivi/{username}")
	public ResponseEntity<List<SuiviDTO>> deleteSuivi(@PathVariable("username") String username, @RequestBody SuiviDTO suivi) throws NotFoundException, AuthorizationException {
		List<Suivi> suivis = this.suiviService.deleteSuivi(username, this.suiviMapper.toSuivi(suivi));
		return ResponseEntity.ok(this.suiviMapper.toListeSuiviDTO(suivis));
	}
	
}
