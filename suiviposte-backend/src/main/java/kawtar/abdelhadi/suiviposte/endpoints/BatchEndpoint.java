package kawtar.abdelhadi.suiviposte.endpoints;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/batch")
public class BatchEndpoint {

	 @Autowired
	 JobLauncher jobLauncher;

	 @Autowired
	 Job batchJobStepSuiviPoste;
	 
	 @Autowired
	 JobRepository jobRepository;
	 
	 @GetMapping(value = "/jobSuiviPoste")
	 public ResponseEntity<String> launchJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
			this.jobLauncher.run(batchJobStepSuiviPoste, new JobParameters());
			
			return ResponseEntity.ok("Vous pouvez consulter l'état du batch : http://localhost:8080/suiviposte/batch/jobSuiviPosteState/" + batchJobStepSuiviPoste.getName());
	}
	 
	 @GetMapping(value = "/jobSuiviPosteState/{jobName}")
	 public ResponseEntity<JobExecution> getJobState(@PathVariable("jobName") String jobName) {
		 JobExecution jobExecution = this.jobRepository.getLastJobExecution(jobName, new JobParameters());
		 return ResponseEntity.ok(jobExecution);
	 }
	 
}
