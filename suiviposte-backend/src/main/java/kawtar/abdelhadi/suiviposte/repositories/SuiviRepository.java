package kawtar.abdelhadi.suiviposte.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kawtar.abdelhadi.suiviposte.entities.Suivi;

@Repository
public interface SuiviRepository extends JpaRepository<Suivi, Long>{

}
