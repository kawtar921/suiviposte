package kawtar.abdelhadi.suiviposte.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.exceptions.APIResponseException;

@Configuration
@EnableBatchProcessing
public class SuiviBatchConfig {
	
	@Autowired
	JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	StepBuilderFactory stepBuilderFactory;
	
	@Bean
	public SuiviBatchWriter batchWriter() {
		return new SuiviBatchWriter();
	}
	
	@Bean
	public SuiviBatchProcessor batchProcessor() {
		return new SuiviBatchProcessor();
	}
	
	@Bean
	public SuiviBatchReader batchReader() {
		return new SuiviBatchReader();
	}
	
	@Bean
	public Step batchStepSuiviPoste() {
		return stepBuilderFactory.get("batchStepSuiviPoste")
				.<Suivi, Suivi>chunk(10).reader(batchReader()).processor(batchProcessor())
				.writer(batchWriter()).faultTolerant().skip(Exception.class).build();
	}

	@Bean
	public Job batchJobStepSuiviPoste() throws Exception {
		return jobBuilderFactory.get("batchJobStepSuiviPoste")
				.start(batchStepSuiviPoste()).build();
	}
}
