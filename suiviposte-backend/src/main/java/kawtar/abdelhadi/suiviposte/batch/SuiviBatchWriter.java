package kawtar.abdelhadi.suiviposte.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.services.SuiviService;

public class SuiviBatchWriter implements ItemWriter<Suivi>{

	@Autowired 
	private SuiviService suiviService;
	
	@Override
	public void write(List<? extends Suivi> items) throws Exception {
		this.suiviService.saveAllSuiviForBatch((List<Suivi>) items);
	}

}
