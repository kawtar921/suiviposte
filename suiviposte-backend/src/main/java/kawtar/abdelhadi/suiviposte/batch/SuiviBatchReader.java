package kawtar.abdelhadi.suiviposte.batch;

import java.util.Iterator;

import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.services.SuiviService;

public class SuiviBatchReader implements ItemReader<Suivi>{
	
	@Autowired SuiviService suiviService;
	
	
	private Iterator<Suivi> suiviIterator;
	

	@Override
	public Suivi read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if(suiviIterator != null && !suiviIterator.hasNext()) {
			return suiviIterator.next();
		}
		
		return null;
	}
	
	@BeforeStep
	public void init() {
		suiviIterator = this.suiviService.getAllSuiviForBatch().iterator();
	}

	
}
