package kawtar.abdelhadi.suiviposte.batch;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import kawtar.abdelhadi.suiviposte.beans.PosteAPIBean;
import kawtar.abdelhadi.suiviposte.beans.TimelineEntryBean;
import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.enums.StatutSuivi;
import kawtar.abdelhadi.suiviposte.services.PosteAPIService;

public class SuiviBatchProcessor implements ItemProcessor<Suivi,Suivi>{

	@Autowired PosteAPIService posteAPIService;
	
	@Override
	public Suivi process(Suivi item) throws Exception {
		
		PosteAPIBean posteAPIBean = this.posteAPIService.getPosteAPIBean(item.getCodeSuivi());
		
		Long etatBD = item.getStatut().getId();
		Map<Integer,TimelineEntryBean> etatsPoste = posteAPIBean.getShipment().getTimeline().stream().collect(Collectors.toMap(TimelineEntryBean::getId, Function.identity()));
		
		for(StatutSuivi statut : StatutSuivi.values()) {
			if(statut.isPoste() && etatsPoste.get(statut.getId().intValue()).getStatus() && statut.getId() > etatBD) {
				//Nouveau état
				item.setStatut(StatutSuivi.getStatutSuiviById(statut.getId()));
			}
		}
		
		return item;
	}

}
