package kawtar.abdelhadi.suiviposte.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import kawtar.abdelhadi.suiviposte.dto.SuiviDTO;
import kawtar.abdelhadi.suiviposte.entities.Suivi;

@Mapper(componentModel = "spring")
public interface SuiviMapper {
	
	@Mapping(target = "libelleStatut", source = "suivi.statut.libelle")
	@Mapping(target = "codeStatut", source = "suivi.statut.id")
	SuiviDTO toSuiviDTO(Suivi suivi);
	
	List<SuiviDTO> toListeSuiviDTO(List<Suivi> suivis);
	
	@Mapping(target = "statut", expression = "java(StatutSuivi.getStatutSuiviById(suiviDTO.getCodeStatut()))")
	Suivi toSuivi(SuiviDTO suiviDTO);

}
