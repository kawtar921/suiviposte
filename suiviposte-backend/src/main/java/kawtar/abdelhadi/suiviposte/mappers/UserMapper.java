package kawtar.abdelhadi.suiviposte.mappers;

import org.mapstruct.Mapper;

import kawtar.abdelhadi.suiviposte.dto.UserDTO;
import kawtar.abdelhadi.suiviposte.entities.User;

@Mapper(componentModel = "spring", uses = SuiviMapper.class)
public interface UserMapper {

	UserDTO toUserDTO(User user);
	
	User toUser(UserDTO userDTO);
}
