package kawtar.abdelhadi.suiviposte.mappers;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import kawtar.abdelhadi.suiviposte.dto.ExceptionDTO;

@Mapper(componentModel = "spring", 
		nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT)
public abstract class ExceptionMapper {
	
	private static final String INTERNAL_ERROR_MESSAGE = "Une erreur interne est survenue, veuillez réessayer plus tard";

	@Mapping(target="message")
	@Mapping(target="codeError", source="code")
	public abstract ExceptionDTO toExceptionDTO(String message, int code);
	
	
	@AfterMapping
	public ExceptionDTO doAfterMapping(String message , int code, @MappingTarget ExceptionDTO exception) {
	    if (exception == null || code == 500 || message == null || message.isEmpty()) {
	    	exception = new ExceptionDTO();
	    	exception.setCodeError("500");
	    	exception.setMessage(INTERNAL_ERROR_MESSAGE);
	    }
	    return exception;
	}
}
