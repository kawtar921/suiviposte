package kawtar.abdelhadi.suiviposte.dto;

import lombok.Data;

@Data
public class SuiviDTO {
	
	private Long idSuivi;
	
	private Long codeStatut;
	
	private String libelleStatut;
	
	private String codeSuivi;
	
	private Boolean notifEmail;
	
	private Boolean notifTelephone;

}
