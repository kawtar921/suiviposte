package kawtar.abdelhadi.suiviposte.dto;

import java.util.List;

import lombok.Data;

@Data
public class UserDTO {
	
	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String telephone;
	private List<SuiviDTO> suivis;

}
