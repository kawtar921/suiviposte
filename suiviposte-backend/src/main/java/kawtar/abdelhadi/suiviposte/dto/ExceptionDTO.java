package kawtar.abdelhadi.suiviposte.dto;

import lombok.Data;

@Data
public class ExceptionDTO {
	
	private String message;
	private String codeError;

}
