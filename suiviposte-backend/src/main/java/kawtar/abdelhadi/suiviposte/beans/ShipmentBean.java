package kawtar.abdelhadi.suiviposte.beans;

import java.util.List;

import lombok.Data;

@Data
public class ShipmentBean {
	
	List<TimelineEntryBean> timeline;
}
