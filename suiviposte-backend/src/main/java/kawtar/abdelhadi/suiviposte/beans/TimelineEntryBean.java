package kawtar.abdelhadi.suiviposte.beans;

import lombok.Data;

@Data
public class TimelineEntryBean {
	
	private Integer id;
	private String shortLabel;
	private Boolean status;
}
