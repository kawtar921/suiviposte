package kawtar.abdelhadi.suiviposte.beans;

import lombok.Data;

@Data
public class PosteAPIBean {
	
	private Integer returnCode;
	private ShipmentBean shipment;

}
