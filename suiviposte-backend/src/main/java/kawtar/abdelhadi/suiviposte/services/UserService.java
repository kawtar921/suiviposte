package kawtar.abdelhadi.suiviposte.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kawtar.abdelhadi.suiviposte.entities.User;
import kawtar.abdelhadi.suiviposte.exceptions.AuthorizationException;
import kawtar.abdelhadi.suiviposte.exceptions.NotFoundException;
import kawtar.abdelhadi.suiviposte.repositories.UserRepository;

@Service
public class UserService {
	
	@Autowired UserRepository userRepository;

	public User connect(String username, String password) throws AuthorizationException, NotFoundException {
		User user = this.getUserById(username);
		if(password.equals(user.getPassword())) {
			return user;
		} else {
			throw new AuthorizationException("Le mot de passe est invalide");
		}
		
	}
	
	public User addOrUpdateUser(User user) {
		return this.userRepository.save(user);
	}
	
	public User getUserById(String username) throws NotFoundException {
		Optional<User> optional = this.userRepository.findById(username);
		if(optional.isPresent()) {
			return optional.get();
		}
		throw new NotFoundException("Ce user n'existe pas");
		
	}
	
	
}
