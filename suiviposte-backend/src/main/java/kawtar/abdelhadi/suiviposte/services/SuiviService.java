package kawtar.abdelhadi.suiviposte.services;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import kawtar.abdelhadi.suiviposte.entities.Suivi;
import kawtar.abdelhadi.suiviposte.entities.User;
import kawtar.abdelhadi.suiviposte.enums.StatutSuivi;
import kawtar.abdelhadi.suiviposte.exceptions.NotFoundException;
import kawtar.abdelhadi.suiviposte.repositories.SuiviRepository;

@Service
public class SuiviService {
	
	@Autowired JdbcTemplate jdbcTemplate;
	@Autowired UserService userService;
	@Autowired SuiviRepository suiviRepository;
	
	private static final String SQL_GET_ALL_SUIVI = "select * from suivi";
	private static final String SQL_SET_ALL_SUIVI = "update suivi set statut = ? where id_suivi = ?";

	public List<Suivi> getAllSuiviByUserId(String username) throws NotFoundException{
		User user = this.userService.getUserById(username);
		return user.getSuivis();
	}
	
	public List<Suivi> addSuivi(String username,Suivi suivi) throws NotFoundException {
		User user = this.userService.getUserById(username);
		suivi.setStatut(StatutSuivi.CREE);
		user.getSuivis().add(suivi);
		return this.userService.addOrUpdateUser(user).getSuivis();
	}
	
	public Suivi getSuiviById(Long idSuivi) throws NotFoundException {
		Optional<Suivi> optional = this.suiviRepository.findById(idSuivi);
		if(optional.isPresent()) {
			return optional.get();
		}
		
		throw new NotFoundException("Le suivi n° " + idSuivi + " n'existe pas");
	}
	
	public List<Suivi> editSuivi(String username, Suivi suivi) throws NotFoundException{
		User user = this.userService.getUserById(username);
		this.getSuiviById(suivi.getIdSuivi());
		
		suivi.setUser(user);
		suiviRepository.save(suivi);
		
		return this.getAllSuiviByUserId(username); 
	}
	
	public List<Suivi> deleteSuivi(String username, Suivi suivi) throws NotFoundException {
		this.getSuiviById(suivi.getIdSuivi());
	    this.suiviRepository.delete(suivi);
	    
	    return this.getAllSuiviByUserId(username);
	}
	
	public List<Suivi> getAllSuiviForBatch(){
		
		return jdbcTemplate.query(
				SQL_GET_ALL_SUIVI,
	            (rs, rowNum) ->
	                    new Suivi(
	                            rs.getLong("id_suivi"),
	                            StatutSuivi.getStatutSuiviById(rs.getLong("statut")),
	                            rs.getString("code_suivi"),
	                            rs.getBoolean("notif_email"),
	                            rs.getBoolean("notif_telephone"),
	                            null
	                    )
	    );
	}
	
	public void saveAllSuiviForBatch(List<Suivi> suivis) {
		this.jdbcTemplate.batchUpdate(SQL_SET_ALL_SUIVI, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, suivis.get(i).getStatut().getId());
				ps.setLong(2, suivis.get(i).getIdSuivi());
			}

			@Override
			public int getBatchSize() {
				return suivis.size();
			}
			
		});
	}
}
