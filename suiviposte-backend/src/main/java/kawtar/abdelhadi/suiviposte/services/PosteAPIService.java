package kawtar.abdelhadi.suiviposte.services;

import java.text.MessageFormat;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import kawtar.abdelhadi.suiviposte.beans.PosteAPIBean;
import kawtar.abdelhadi.suiviposte.exceptions.APIResponseException;

@Service
@PropertySource("suiviposte.properties")
public class PosteAPIService {
	
	@Value("${poste.api.url}")
	private String urlPoste;
	
	@Value("${poste.api.key}")
	private String apiKeyPoste;
	
	private static final String HEADER_KEY_NAME = "X-Okapi-Key";
	
	public PosteAPIBean getPosteAPIBean(String numeroSuivi) throws APIResponseException {
		String url = MessageFormat.format(urlPoste, numeroSuivi);
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.add(HEADER_KEY_NAME, apiKeyPoste);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(null, headers);

		
		ResponseEntity<PosteAPIBean> response = null;
		
		
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, request, PosteAPIBean.class);
		} catch(HttpStatusCodeException e) {
			throw new APIResponseException("L'api de la Poste a retourné une erreur avec le code : " + e.getRawStatusCode() + " , message : " + e.getResponseBodyAsString());
		}
		
		
		return response.getBody();
		
	}

}
