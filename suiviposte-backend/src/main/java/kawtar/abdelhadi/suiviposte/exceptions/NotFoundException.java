package kawtar.abdelhadi.suiviposte.exceptions;

public class NotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5170215531912851382L;
	
	public NotFoundException(String message) {
		super(message);
	}

}
