package kawtar.abdelhadi.suiviposte.exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import kawtar.abdelhadi.suiviposte.dto.ExceptionDTO;
import kawtar.abdelhadi.suiviposte.mappers.ExceptionMapper;
import lombok.extern.slf4j.Slf4j;


@ControllerAdvice
@Slf4j
public class SuiviPosteExceptionHandler extends ResponseEntityExceptionHandler  {
	
	@Autowired ExceptionMapper exceptionMapper;

	
	@ExceptionHandler({ NotFoundException.class, AuthorizationException.class, ArgumentInvalideException.class})
	@ResponseBody
	protected ResponseEntity<ExceptionDTO> handleAnyException(Exception ex, WebRequest request){

		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String message = ex.getMessage();
		
		if(ex instanceof NotFoundException) {
			status = HttpStatus.NOT_FOUND;
		}
		
		else if(ex instanceof AuthorizationException) {
			status = HttpStatus.UNAUTHORIZED;
		}
		
		else if(ex instanceof ArgumentInvalideException) {
			status = HttpStatus.FORBIDDEN;
		}
		
		else if(ex instanceof APIResponseException) {
			message = "L'api de la Poste n'est pas disponible en ce moment, veuillez rééssayer plus tard.";
		}
		
		

		ExceptionDTO dto = this.exceptionMapper.toExceptionDTO(message, status.value());
		
		log.error("Une erreur est survenue de type {} cause : {} , avec le message suivant : {}" , ex.getClass() , ex.getCause(), ex.getStackTrace());
		return new ResponseEntity<>(dto, status);
	}
}
