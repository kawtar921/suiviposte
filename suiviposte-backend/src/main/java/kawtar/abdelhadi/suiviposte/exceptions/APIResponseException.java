package kawtar.abdelhadi.suiviposte.exceptions;

public class APIResponseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -424928887295311986L;
	
	public APIResponseException(String message) {
		super(message);
	}

}
