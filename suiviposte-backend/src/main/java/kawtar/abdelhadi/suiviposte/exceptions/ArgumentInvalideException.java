package kawtar.abdelhadi.suiviposte.exceptions;

public class ArgumentInvalideException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1697942213612212264L;

	public ArgumentInvalideException(String message) {
		super(message);
	}
}
