package kawtar.abdelhadi.suiviposte.exceptions;

public class AuthorizationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2401831976460366959L;

	public AuthorizationException(String message) {
		super(message);
	}
}
