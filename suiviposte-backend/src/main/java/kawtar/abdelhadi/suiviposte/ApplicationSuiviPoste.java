package kawtar.abdelhadi.suiviposte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import kawtar.abdelhadi.suiviposte.exceptions.SuiviPosteExceptionHandler;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories
@Import(SuiviPosteExceptionHandler.class)
@EntityScan(basePackages = {"kawtar.abdelhadi.suiviposte.entities"})
@ComponentScan("kawtar.abdelhadi.suiviposte")
public class ApplicationSuiviPoste {
	
	public static void main(String[] args) {
	    SpringApplication.run(ApplicationSuiviPoste.class, args);
	}
	
}