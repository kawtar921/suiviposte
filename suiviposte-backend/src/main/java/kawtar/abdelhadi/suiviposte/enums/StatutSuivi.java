package kawtar.abdelhadi.suiviposte.enums;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum StatutSuivi {
	
	CREE(0L, "Le suivi a bien été créé", false),
	PRIS_EN_CHARGE(1L,"Le colis a été pris en charge par la poste", true),
	EN_COURS_ACHEMINEMENT(2L, "Le colis est en cours d'acheminement", true),
	ARRIVE_SITE_DISTRIBUTION(3L, "Le colis est arrivé dans le site en vue de sa distribution", true),
	EN_DISTRIBUTION(4L,"Le colis est en distibution", true),
	DISTRIBUE(5L, "Le colis a été distribué", true),
	CLOS(6L, "Le suivi est clôturé", false);
	
	private String libelle;
	private Long id;
	private boolean poste;
	
	public static final int etatMaximal = 5;
	
	private StatutSuivi(Long id, String libelle, boolean poste) {
		this.libelle = libelle;
		this.id = id;
		this.poste = poste;
	}
	
	public static StatutSuivi getStatutSuiviById(Long id) {
		return Arrays.asList(StatutSuivi.values()).stream().
				filter(statutEnum -> statutEnum.getId() == id).findFirst().get();
	}
}
