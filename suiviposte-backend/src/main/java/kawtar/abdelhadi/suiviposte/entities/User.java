package kawtar.abdelhadi.suiviposte.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class User {
	
	@Id
	@Column(length = 64)
	private String username;
	
	private String password;
	
	private String firstname;
	
	private String lastname;
	
	private String email;
	
	private String telephone;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "user_username")
	private List<Suivi> suivis;
	
}

