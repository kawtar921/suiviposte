package kawtar.abdelhadi.suiviposte.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import kawtar.abdelhadi.suiviposte.enums.StatutSuivi;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Suivi {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idSuivi;
	
	private StatutSuivi statut;
	
	private String codeSuivi;
	
	private Boolean notifEmail;
	
	private Boolean notifTelephone;
	
	@OneToOne
	private User user;
}
